<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPlaces extends Model
{
    protected $table = 'users_places';
    public $timestamps = false;
}
