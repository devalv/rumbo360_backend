# ************************************************************
# Sequel Pro SQL dump
# Versi�n 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: sql9.freemysqlhosting.net (MySQL 5.5.58-0ubuntu0.14.04.1)
# Base de datos: sql9247867
# Tiempo de Generaci�n: 2018-07-16 13:51:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `category` varchar(100) NOT NULL DEFAULT '',
  `location` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;

INSERT INTO `places` (`id`, `name`, `category`, `location`, `address`, `info`)
VALUES
	(1,'Papas de la maría','Comida','-0.115228, -78.487762','Av. Bellavista y Ciruelos','Excelentes Papas con cuero'),
	(2,'Museo de la muerte','Cultura','-0.115228, -78.487762','De los Eucaliptos N34-312','Museo culutural sobre antiguas creencias de la muerte'),
	(3,'Estadio Liga Barrial Ofelia','Diversión','-0.110223, -78.490245','Av. Diego de Vasquez y Kennedy','Estadio de ligas barriales con cesped sintetico'),
	(4,'TGI Saturdays','Comida','-0.110222, -78.490242','Av. Diego de Vasquez y Av. Marisol','Excelente restaurante para ir un domingo');

/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `name`)
VALUES
	(1,'a@b.com','12345','Prueba'),
	(2,'juanca@mail.com','12345','Juanca Muñoz'),
	(3,'a@c.com','12345','Pancho Alvear');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users_places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_places`;

CREATE TABLE `users_places` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `placeid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usersId` (`uid`),
  KEY `fk_placesId` (`placeid`),
  CONSTRAINT `fk_placesId` FOREIGN KEY (`placeid`) REFERENCES `places` (`id`),
  CONSTRAINT `fk_usersId` FOREIGN KEY (`uid`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

LOCK TABLES `users_places` WRITE;
/*!40000 ALTER TABLE `users_places` DISABLE KEYS */;

INSERT INTO `users_places` (`id`, `uid`, `placeid`)
VALUES
	(1,1,1),
	(2,2,1),
	(5,3,1),
	(7,1,2),
	(8,2,3),
	(9,3,3),
	(10,2,2);

/*!40000 ALTER TABLE `users_places` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
